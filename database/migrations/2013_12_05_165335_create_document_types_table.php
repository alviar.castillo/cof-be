<?php

use App\DocumentType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('tipo de documento')->unique();
            $table->timestamps();
        });

        $d1 = DocumentType::create(['name' => 'C.C']);
        $d2 = DocumentType::create(['name' => 'C.E']);
        $d3 = DocumentType::create(['name' => 'Pasaporte']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_types');
    }
}
