<?php

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->comment('Correo Electronico');
            $table->timestamp('email_verified_at')->comment('Fecha Correo verificado')->nullable();
            $table->string('password')->comment('Contraseña');
            $table->string('uid')->comment('uid')->nullable();
            $table->string('first_name')->comment('primer nombre');
            $table->string('middle_name')->nullable()->comment('segundo nombre');
            $table->string('last_name')->comment('primer apellido');
            $table->string('second_last_name')->nullable()->comment('segundo apelido');
            $table->string('document_number',20)->nullable()->comment('documento de identidad');
            $table->string('contact_number',15)->nullable()->comment('numero de contacto');
            $table->date('birth_date')->nullable()->comment('fecha del nacimiento');
            $table->string('gender')->nullable()->comment('genero');
            $table->string('marital_status')->nullable()->comment('estado civil');
            $table->string('address')->nullable()->comment('direccion');
            $table->unsignedBigInteger('role_id')->comment('Rol');
            $table->unsignedBigInteger('document_type_id')->nullable()->comment('tipo de documento');
            $table->unsignedBigInteger('localization_id')->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('image_id')->nullable();
            $table->unsignedBigInteger('kinship_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->unique(['email','document_number']);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('localization_id')->references('id')->on('localizations');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('image_id')->references('id')->on('files');
            $table->foreign('kinship_id')->references('id')->on('kinships');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(
            [
                'first_name' => 'Administrador',
                'last_name' => 'Administrador',
                'email' => 'admin@admin.com',
                'password' => bcrypt('administrador'),
                'role_id' => Role::where('name','Administrador')->first()->id,
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
                'portal_id' => 1
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
