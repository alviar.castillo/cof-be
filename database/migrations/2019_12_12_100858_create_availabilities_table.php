<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date')->comment('fecha');
            $table->date('day')->comment('dia');
            $table->dateTime('start')->comment('inicio de disponibilidad');
            $table->dateTime('end')->comment('finak de disponibilidad');
            $table->integer('duration')->comment('duracion');
            $table->boolean('recurrent');
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('assesor_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->unique(['start','end']);
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('assesor_id')->references('id')->on('assesors');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilities');
    }
}
