<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typology extends Model
{
    const NAME_SECTION = 'TIPOLOGIA';

    protected $fillable = [
        'name','portal_id'
    ];

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
}
