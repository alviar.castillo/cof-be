<?php

namespace App\Http\Resources;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Resources\Json\JsonResource;

class UserAuth extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
                  
        $credentials = [
            'email' => $this->email,
            'password' => env('pass_user'),
            
        ];
        
        $token = JWTAuth::attempt($credentials);

        return [
            'token' => $token,
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'second_last_name' => $this->second_last_name,
            'email' => $this->email,
            'uid' => $this->uid,
            'document_type_id' => $this->document_type->name,
            'document_number' => $this->document_number,
            'contact_number' => $this->contact_number,
        ];
    }
}
