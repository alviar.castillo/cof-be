<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesorStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesor_studies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', [
                'Tecnico','Tecnologo','Profesional',
                'Especializacion','Maestria','Doctorado'])->comment('nivel academico');
            $table->date('graduation_year')->comment('año de graduacion');
            $table->string('obtained_title')->comment('titulo obtenido');
            $table->string('college')->comment('universidad');
            $table->unsignedBigInteger('assesor_id')->nullable();
            $table->foreign('assesor_id')->references('id')->on('assesors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesor_studies');
    }
}
