<?php

namespace App\Mail;

use App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @var  password_reset
     * @var  user
     * 
     */
    public $password_reset;
    public $user;
    
  


    public function __construct($password_reset, $user)
    {
        $this->password_reset = $password_reset;
        $this->user = $user;
    }
   
   

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ResetPassword');
    }
}
