<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    const NAME_SECTION = 'HABILIDAD';

    protected $fillable = [
        'name','is_active','portal_id'
    ];

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    public function assesor()
    {
        return $this->belongsToMany('App\Assesor');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }
}
