<?php

use Illuminate\Support\Facades\Route;


Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login')->name('auth.login');
    Route::post('logout', 'AuthController@logout')->name('auth.logout');
});

Route::post('/upload-image', 'UploadController@storeImage')->name('storeImage');
Route::post('/upload-file', 'UploadController@uploadFile')->name('storeFile');

Route::post('/register', 'LoginController@register')->name('user.register');
Route::post('/login', 'LoginController@login')->name('user.login');
Route::post('/refresh', 'LoginController@refresh')->name('user.refresh');
Route::post('/restore', 'LoginController@restore')->name('user.restore');
Route::post('/validate/{token}', 'LoginController@valid')->name('user.valid');
Route::post('/reset/{token}', 'LoginController@reset')->name('user.reset');
