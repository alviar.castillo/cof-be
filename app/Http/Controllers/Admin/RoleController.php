<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use App\LogUser;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $models = Role::whereNotIn('name',['Administrador','Empleado','beneficiario','Especialista'] )->orderBy('id','asc')->paginate(15);
        
        return  $models;
    }
    
    public function store(Request $request)
    {
        
        $validator = validator($request->all(), [
            'name' => 'required|string|max:190|unique:roles,name',
            'permissions_id' => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $role = new Role();
        $role->fill($request->all());
        $role->save();

        $role->permissions()->sync($request->permissions_id);

        $actual_value= json_encode($role->load('permissions'));
        $action = LogUser::CREATE;
        $section = Role::NAME_SECTION;
        $object = $role;
        $this->create_log( $action, $section, $object, $actual_value);

        return response()->json(['model' => $role, 'success' => true], 201);
    }

    public function update(Request $request, Role $role)
    {
        $validator = validator($request->all(), [
            'name' => 'required|string|max:190|unique:roles,name,'.$role->id,
            'permissions_id' => 'required|array',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
            }
        
        $role1 = json_encode($role->load('permissions'));
        
        $role->fill($request->all());
        $role->save();

        $role->permissions()->sync($request->permissions_id);

        $initial_value= $role1;
        $actual_value= json_encode($role->load('permissions'));
        $action = LogUser::EDIT;
        $section = Role::NAME_SECTION;
        $object = $role;
        $this->create_log( $action, $section, $object,$actual_value, $initial_value );
        
       

        return response()->json(['model' => $role, 'success' => true], 201);
    }

    public function show(Request $request, Role $role)
    {
        if(!in_array($role->name, ['SuperAdmin','Administrador','Usuario'])){
            $permissions_id=[];
            foreach ($role->permissions as $key) {
                $permissions_id[]=$key->pivot->permission_id;
            }
            
            $role['permissions_id']=$permissions_id;
            
            return ($role);
        }
        return null;
        
    }
    
    public function permissions(Request $request)
    {
        $models = Permission::orderBy('id','asc')->get();
       
        return  $models;
    }
}
