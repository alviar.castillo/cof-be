<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppoinmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appoinments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['Virtual','Presencial'])->comment('tipo de cita');
            $table->text('reason')->comment('motivos de la consulta');
            $table->decimal('qualification',3,2)->nullable()->comment('calificacion');
            $table->text('qualification_comment')->nullable()->comment('comentario calificacion');
            $table->string('call_link')->nullable()->comment('link de videollamada');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('assesor_id')->nullable();
            $table->unsignedBigInteger('availability_id')->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->unsignedBigInteger('typology_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assesor_id')->references('id')->on('assesors');
            $table->foreign('availability_id')->references('id')->on('availabilities');
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('typology_id')->references('id')->on('typologies');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appoinments');
    }
}
