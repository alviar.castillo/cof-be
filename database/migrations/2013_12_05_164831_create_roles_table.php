<?php

use App\Role;
use App\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Nombre Rol')->unique();
            $table->timestamps();
        });

        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Nombre Permiso')->unique();
            $table->timestamps();
        });

        Schema::create('role_permission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('permission_id');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->unique(['role_id', 'permission_id']);
        });
       
        $r1 = Role::create(['name' => 'Administrador']);
        $r2 = Role::create(['name' => 'Empleado']);
        $r2 = Role::create(['name' => 'Beneficiario']);
        $r2 = Role::create(['name' => 'Especialista']);

        $p1 = Permission::create(['name' => 'usuarios']);
        $p2 = Permission::create(['name' => 'roles']);
        $p3 = Permission::create(['name' => 'logUsuario']);
        $p4 = Permission::create(['name' => 'detalleLogUsuario']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permission');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
    }
}
