<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('nombre del servicio');
            $table->boolean('for_beneficiary')->comment('indica si aplica para beneficiarios');
            $table->boolean('for_younger')->default(false)->nullable()->comment('indica si aplica para menores de edad');
            $table->boolean('is_active')->comment('indica si esta activo');
            $table->smallInteger('user_limit')->comment('indica el limite de citas por usuario al año');
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->unique(['name']);
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialties');
    }
}
