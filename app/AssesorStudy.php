<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssesorStudy extends Model
{
    const NAME_SECTION = 'ESTUDIO ASESOR';

    const TECHNICAL = 'Tecnico';
    const TECHNOLOGIST = 'Tecnologo';
    const PROFESSIONAL = 'Profesional';
    const SPECIALIZATION = 'Especializacion';
    const MASTER = 'Maestria';
    const DOCTORATE = 'Doctorado';

    protected $fillable = [
        'type','graduation_year','obtained_title','college'
    ];
}
