<?php

use App\Kinship;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKinshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('parentesco');
            $table->timestamps();
        });

        $k1 = Kinship::create(['name' => 'Padre']);
        $k2 = Kinship::create(['name' => 'Madre']);
        $k3 = Kinship::create(['name' => 'Hijo']);
        $k4 = Kinship::create(['name' => 'Hermano']);
        $k5 = Kinship::create(['name' => 'Esposo']);
        $k6 = Kinship::create(['name' => 'Esposa']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kinships');
    }

    
}
