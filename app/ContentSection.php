<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentSection extends Model
{
    protected $fillable = [
        'type','title','description','url','image_id','portal_id',
    ];

    public function image(){
        return $this->hasOne('App\File', 'id', 'image_id');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }
}
