<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const NAME_SECTION = 'ROL';

    protected $fillable = [
        'name' 
    ];

    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'role_permission');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }
}
