<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\HabeasData;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Exports\HabeasDataExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Builder;


class HabeasDataController extends Controller
{
    public function index()
    {
        $models = HabeasData::with('user')->paginate(15);
        
        return $models;

    }

    public function export(Request $request) 
    {  

        $type = $request->type;
        $value = $request->value;
        $email = $request->email;
        $date= $request->date;
        
    
        $models = HabeasData::with('user')
                ->when($type, function ($query, $type) {
                    return $query->where('type', $type);
                })
                ->when($value, function ($query, $value) {
                    if ($value=='no') {
                        $value=false;
                    }else{
                        $value=true;}
                    return $query->where('value', $value);
                })
                ->when($date, function ($query, $date) {
                    $date_i= new carbon($date[0]);
                    $date_f= new carbon($date[1]);
          
                    return $query->whereBetween('updated_at', [$date_i,$date_f]);
                })
                ->when($email, function ($query, $email) {
                    return $query->where('email', $email);
                })
                ->get();
         
        

        return Excel::download(new HabeasDataExport($models), 'habeas_data.xlsx');
    }

    public function exportSelected(Request $request) 
    {   
        $models= Habeasdata::whereIn('id', $request->all())->get();
        return Excel::download(new HabeasDataExport($models), 'habeas_data.xlsx');
    }
    
    public function filters($name = '')
    {
        return  User::where('email', 'like', '%'.$name.'%')
                    ->whereHas('role', function (Builder $query) {
                        $query->where('name', 'Usuario');
                    })
                    ->get();
        
    }
}
