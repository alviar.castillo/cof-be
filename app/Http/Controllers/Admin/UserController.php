<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use App\LogUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $models= User::with('role')->paginate(15);
    
        return $models;
    }
    
    public function store(Request $request)
    {
        
        $validator = validator($request->all(), [
            'first_name' => 'required|string|max:190',
            'last_name' => 'required|string|max:190',
            'email' => 'required|email|unique:users,email',
            'role' => 'required',
            'password' => 'required|string|min:6|max:12',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $user = new User();
        $datos = $request->all();
        $datos['password'] = bcrypt($request->password);
        $datos['role_id'] = $request->role['id'];
        $user->fill($datos);
        $user->save();
        
        $actual_value= json_encode($user->load('role'));
        $action = LogUser::CREATE;
        $section = User::NAME_SECTION;
        $user['name'] = $user->first_name;
        $object = $user;
        $this->create_log( $action, $section, $object, $actual_value);

        return response()->json(['model' => $user, 'success' => true], 201);
    }

    public function update(Request $request, User $user)
    {
        $validator = validator($request->all(), [
            'first_name' => 'required|string|max:190',
            'last_name' => 'required|string|max:190',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'role' => 'required',
            'password' => 'nullable|string|min:6|max:12',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
            }
        
        $user1 = json_encode($user->load('role'));
        
        $datos = $request->all();
        if ($request->password) {
            $datos['password'] = bcrypt($request->password);
        }
        $datos['role_id'] = $request->role['id'];
        $user->fill($datos);
        $user->save();


        $initial_value= $user1;
        $actual_value= json_encode($user->load('role'));
        $action = LogUser::EDIT;
        $section = User::NAME_SECTION;
        $user['name'] = $user->first_name;
        $object = $user;
        $this->create_log( $action, $section, $object,$actual_value, $initial_value );
        
       

        return response()->json(['model' => $user, 'success' => true], 201);
    }

    public function show(Request $request, User $user)
    {
        $user['role']=$user->role;
        return  $user;
    }
    
    public function roles(Request $request)
    {
        $models = Role::whereNotIn('name', ['Empleado','Beneficiario','Administrador'])->orderBy('id','asc')->get();
        
        return  $models;
    }
}
