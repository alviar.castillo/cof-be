<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
    const NAME_SECTION = 'PORTALES';

    protected $fillable= [
        'name','duration'
    ];
}
