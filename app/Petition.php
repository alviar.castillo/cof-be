<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petition extends Model
{
    const NAME_SECTION = 'PETICION';

    protected $fillable=[
        'type','json_request','json_response'
    ];
}
