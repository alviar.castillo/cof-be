<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{

    const CREATE = 'CREAR';
    const EDIT = 'MODIFICAR';
    const DELETE = 'ELIMINAR';

    protected $fillable = [
        'user_id','action','section','object_name','initial_value','actual_value',
        'portal_id',
    ];

    public function loguserable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
}
