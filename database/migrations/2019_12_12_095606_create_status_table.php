<?php

use App\Status;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('type',['Cita','Agenda']);
            $table->timestamps();
        });

        $s1 = Status::create(['name'=>'confirmado','type'=>'Cita']);
        $s2 = Status::create(['name'=>'cancelado','type'=>'Cita']);
        $s3 = Status::create(['name'=>'concluida','type'=>'Cita']);
        $s4 = Status::create(['name'=>'perdida','type'=>'Cita']);
        $s5 = Status::create(['name'=>'disponible','type'=>'Agenda']);
        $s6 = Status::create(['name'=>'cancelado','type'=>'Agenda']);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
