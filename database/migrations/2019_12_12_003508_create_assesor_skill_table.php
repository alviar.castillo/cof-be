<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesorSkillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesor_skill', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('assesor_id')->nullable();
            $table->unsignedBigInteger('skill_id')->nullable();
            $table->foreign('assesor_id')->references('id')->on('assesors');
            $table->foreign('skill_id')->references('id')->on('skills');
            $table->unique(['assesor_id', 'skill_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesor_skill');
    }
}
