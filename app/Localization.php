<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{
    const NAME_SECTION = 'LOCALIZACION';

    protected $fillable= [
        'region','departament_code','departament','departament_slug',
        'municipality_code','municipality','municipality_slug'
    ];
}
