/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueRouter from 'vue-router'
import draggable from 'vuedraggable';
import FileUpload from "@/components/FilesUpload/FileUpload.vue";
import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import Multiselect from 'vue-multiselect';

Vue.use(Antd);
Vue.use(VueRouter);
Vue.use(VueQuillEditor)
Vue.component('draggable', draggable);
Vue.component('FileUpload', FileUpload);
Vue.component('template-uploader', require('@/components/TemplateUploaderComponent.vue').default);
Vue.component('multiselect', Multiselect);

Vue.config.devtools = true;

import routes from "@/router";
const router = new VueRouter({
    routes: routes
});

const app = new Vue({
    el: '#app',
    router,
    created() {
        this.$message.config({
            top: "80px",
            duration: 2,
            maxCount: 3
        });
    }
});
