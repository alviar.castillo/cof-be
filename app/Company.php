<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const NAME_SECTION = 'COMPAÑIA';

    protected $fillable= [
        'name','portal_id'
    ];

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
}
