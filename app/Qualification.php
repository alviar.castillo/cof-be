<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    const NAME_SECTION = 'CALIFICACION';

    const AMABILITY = 'Amabilidad';
    const PROFESIONALISM = 'Profesionalismo';
    const DURATION = 'Duracion de la cita';

    protected $fillable = [
        'type','value','description','user_id','appoinment_id',
        'portal_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function appoinment()
    {
        return $this->belongsTo('App\Appoinment');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
}
