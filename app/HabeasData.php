<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HabeasData extends Model
{
    const NAME_SECTION = 'HABEAS DATA';
    const TERM = 'Terminos';
    const COMMERCIAL_INFORMATION = 'Informacion comercial';

    protected $table = 'habeas_data';

    protected $fillable = [
        'user_id','email','ip_address','type','value','portal_id'
    ];

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function scopeType($query, $type){
        if ($type) {
            return $query->where('type', 'LIKE', $type);
        }
    }
    
    public function scopeValue($query, $value){
        if ($value) {
            return $query->where('value', 'LIKE', $value);
        }
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
    

}
