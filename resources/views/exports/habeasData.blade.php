<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Email</th>
        <th>Permiso</th>
        <th>Acepto</th>
        <th>Ip</th>
        <th>Creado</th>
        <th>Actualizado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($habeasData as $data)
        <tr>
            <td>{{ $data->user->id }}</td>
            <td>{{ $data->user->first_name }}</td>
            <td>{{ $data->user->last_name }}</td>
            <td>{{ $data->user->email }}</td>
            <td>{{ $data->type }}</td>
            <td>
               @if($data->value)
               Si
               @else
               No
               @endif
            </td>
            <td>{{ $data->ip_address }}</td>
            <td>{{ $data->created_at }}</td>
            <td>{{ $data->updated_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>