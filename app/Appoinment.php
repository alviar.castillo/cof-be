<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appoinment extends Model
{
    const NAME_SECTION = 'CITAS';
    const VIRTUAL = 'Virtual';
    const PRESENTIAL = 'Presencial';

    protected $fillable = [
        'type','description','qualification','call_link',
        'user_id','assesor_id','availability_id',
        'status_id','typology_id','portal_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assesor()
    {
        return $this->belongsTo('App\Assesor');
    }
    
    public function availability()
    {
        return $this->belongsTo('App\Availability');
    }
    
    
    public function typology()
    {
        return $this->belongsTo('App\Tipology');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'id', 'status_id');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
    
}
