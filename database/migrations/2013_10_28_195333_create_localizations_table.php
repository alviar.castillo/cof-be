<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('localizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('region')->nullable()->comment('region');
            $table->string('departament_code')->nullable()->comment('codigo de departamento');
            $table->string('departament')->nullable()->comment('departamento');
            $table->string('departament_slug')->nullable()->comment(' slug departamento');
            $table->string('municipality_code')->nullable()->comment('codigo de municipio');
            $table->string('municipality')->nullable()->comment('municipio');
            $table->string('municipality_slug')->nullable()->comment(' slug municipio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('localizations');
    }
}
