<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address')->comment('direccion');
            $table->text('description', 500)->comment('perfil');
            $table->decimal('qualification')->nullable()->comment('califiacion');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('specialty_id')->nullable();
            $table->unsignedBigInteger('localization_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->unique(['user_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('specialty_id')->references('id')->on('specialties');
            $table->foreign('localization_id')->references('id')->on('localizations');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesors');
    }
}
