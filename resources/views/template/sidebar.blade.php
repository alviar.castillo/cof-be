<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">

            @if(Auth::user()->hasRole('roles'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'roles'}" :class="['nav-link']">
                    <i class="icon-info"></i>Roles<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('usuarios'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'users'}" :class="['nav-link']">
                    <i class="icon-info"></i>Usuarios<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('logUsuario'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'log-user'}" :class="['nav-link']">
                    <i class="icon-info"></i>Log usuario<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('habeasData'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'habeas-data'}" :class="['nav-link']">
                    <i class="icon-info"></i>Habeas data<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>