<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qualifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['Amabilidad','Profesionalismo','Duracion de la cita'])->comment('tipo de evaluacion');
            $table->decimal('value')->comment('valor');
            $table->decimal('description')->comment('valor');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('appoinment_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->unique('appoinment_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('appoinment_id')->references('id')->on('appoinments');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qualifications');
    }
}
