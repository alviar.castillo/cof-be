<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_petitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['a','b']);
            $table->text('json_request');
            $table->text('json_response');
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_petitions');
    }
}
