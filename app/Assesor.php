<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assesor extends Model
{
    const NAME_SECTION = 'ASESOR';

    protected $filiable = [
        'address','description','qualification','user_id','specialty_id',
        'portal_id','localization_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function specialty()
    {
        return $this->hasOne('App\Specialty');
    }

    public function localization()
    {
        return $this->belongsTo('App\Localization');
    }
    
    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
    
    public function assesor_studies()
    {
        return $this->hasMany('App\AssesorStudy');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }
}
