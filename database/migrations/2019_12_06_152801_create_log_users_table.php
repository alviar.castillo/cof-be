<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('action', ['crear', 'modificar', 'eliminar'])->comment('accion');
            $table->string('section');
            $table->string('object_name');
            $table->text('initial_value')->nullable();
            $table->text('actual_value');
            $table->integer('loguserable_id');
            $table->string('loguserable_type');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_users');
    }
}
