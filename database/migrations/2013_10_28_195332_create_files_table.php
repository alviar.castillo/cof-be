<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('path_file')->nullable()->comment('Archivo');
            $table->string('path')->nullable()->comment('Archivo');
            $table->text('summary')->nullable()->comment('texto alternativo del archivo');
            $table->enum('type', ['image', 'video', 'pdf'])->comment('Tipo de archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
