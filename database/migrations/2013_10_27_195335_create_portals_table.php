<?php

use App\Portal;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('portal');
            $table->integer('duration')->comment('duracion');
            $table->timestamps();
        });

        $p1 = Portal::create(['name' => 'Base Portal', 'duration' => 45]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portals');
    }
}
