<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_sections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['a','b']);
            $table->string('title')->nullable()->comment('titulo');
            $table->string('description')->nullable()->comment('descripcion');
            $table->string('url')->nullable()->comment('url');
            $table->unique(['title']);
            $table->unsignedBigInteger('image_id')->nullable();
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->foreign('image_id')->references('id')->on('files');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_sections');
    }
}
