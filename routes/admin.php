<?php

Route::post('/file/save', 'UploadController@store')->name('file.store');
Route::get('/file/show/{file}', 'UploadController@show')->name('file.show');
Route::post('/file/delete/{file}', 'UploadController@delete')->name('file.delete');

Route::get('/log-user', 'LogUserController@index')->name('log-user.index');
Route::get('/log-user/show/{log_user}', 'LogUserController@show')->name('log-user.show');

Route::get('/user', 'UserController@index')->name('user.index');
Route::post('/user', 'UserController@store')->name('user.store');
Route::put('/user/{user}', 'UserController@update')->name('user.update');
Route::get('/user/show/{user}', 'UserController@show')->name('user.show');
Route::get('/roles', 'UserController@roles')->name('user.roles');

Route::get('/role', 'RoleController@index')->name('role.index');
Route::post('/role', 'RoleController@store')->name('role.store');
Route::put('/role/{role}', 'RoleController@update')->name('role.update');
Route::get('/role/show/{role}', 'RoleController@show')->name('role.show');
Route::get('/permissions', 'RoleController@permissions')->name('role.permissions');

Route::get('/habeas-data', 'HabeasDataController@index')->name('habeas-data.index');
Route::get('/export/habeas-data', 'HabeasDataController@export');
Route::get('/export/habeas-data/selected', 'HabeasDataController@exportSelected');
Route::get('/habeas-data/filters/{name?}', 'HabeasDataController@filters')->name('habeas-data.filters');
