import Home from "@/components/ExampleComponent.vue";

import LogUser from "@/components/LogUser/LogUserComponent.vue";
import LogUserShow from "@/components/LogUser/LogUserShowComponent.vue";

import User from "@/components/User/UserComponent.vue";
import UserForm from "@/components/User/UserFormComponent.vue";

import Role from "@/components/Role/RoleComponent.vue";
import RoleForm from "@/components/Role/RoleFormComponent.vue";

import HabeasData from "@/components/HabeasData/HabeasDataComponent.vue";

export default [
    { path: '/log-user', name: 'log-user', component: LogUser },
    { path: '/log-user/show/:id', name: 'log-user-show', component: LogUserShow },
    { path: '/users', name: 'users', component: User },
    { path: '/users/:type/:id?', name: 'user.form', component: UserForm },
    { path: '/roles', name: 'roles', component: Role },
    { path: '/roles/:type/:id?', name: 'role.form', component: RoleForm },
    { path: '', name: 'index', component: Home },
    { path: '/habeas-data', name: 'habeas-data', component: HabeasData },

];