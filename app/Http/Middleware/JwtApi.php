<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponser;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtApi extends BaseMiddleware
{
    use ApiResponser;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch( \Exception $e ) {
            if( $e instanceof TokenInvalidException ) {
                return $this->errorResponse('Token is Invalid', Response::HTTP_UNAUTHORIZED);
            } else if( $e instanceof TokenExpiredException ) {
                return $this->errorResponse('Token is Expired', Response::HTTP_FORBIDDEN);
            } else {
                return $this->errorResponse('Token not found', Response::HTTP_NOT_FOUND);
            }
        }
        return $next($request);
    }
}
