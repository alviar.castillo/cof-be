<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    const NAME_SECTION = 'ESPECIALIDAD';

    protected $fillable = [
        'name','for_beneficiary','for_younger','is_active',
        'user_limit','portal_id'
    ];

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }
}
