<?php

namespace App\Exports;

use App\HabeasData;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class HabeasDataExport implements FromView
{
   

    public $params;
   
    public function __construct($params)
    {
        $this->params = $params;
    }

    public function view(): View
    {
       return view('exports.habeasData', [
            'habeasData' => $this->params
       ]);
    }
}
