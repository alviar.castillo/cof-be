<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHabeasDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habeas_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->comment('email');
            $table->string('type')->comment('tipo de permiso');
            $table->boolean('value')->comment('valor');
            $table->string('ip_address')->comment('direccion ip');
            $table->unsignedBigInteger('user_id')->nullable()->comment('id de usuario');
            $table->unsignedBigInteger('portal_id')->default(1);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habeas_data');
    }
}
