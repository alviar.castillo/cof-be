<?php

namespace App\Http\Controllers\Api;

use App\Page;
use App\Service;
use App\Traits\ApiResponser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ApiController extends Controller
{
    use ApiResponser;

    public function buildSitemap()
    {
        $data = Cache::remember('sitemap', now()->addMinutes(config('app.minutes_cache')), function () {
            $pages = Page::where(['sitemap' => true, 'is_home' => false])->get();
            $records = [];
            $records[] = $this->builObjectSitemap('');
            foreach ($pages as $page) {
                array_push($records, $this->builObjectSitemap($page->slug));
            }
            $services = Service::where(['is_visible' => true])->get();
            foreach ($services as $service) {
                array_push($records, $this->builObjectSitemap($service->slug));
            }

            return $records;
        });

        return $this->showData($data, 200);
    }

    public function builObjectSitemap($url, $freq = 'daily', $priority = '0.9')
    {
        $data = [];
        $data['url'] = $url;
        $data['lastmod'] = now()->format('Y-m-d');
        $data['changefreq'] = $freq;
        $data['priority'] = $priority;

        return $data;
    }
}
