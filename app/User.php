<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    const NAME_SECTION = 'USUARIO';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','email_verified_at','password','uid','role_id','first_name',
        'middle_name','last_name','second_last_name','document_type_id',
        'document_number','contact_number','birth_date','gender','marital_status',
        'localization_id','company_id','image_id','kinship_id','user_id','portal_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    
    public function document_type()
    {
        return $this->belongsTo('App\DocumentType');
    }
    
    public function localization()
    {
        return $this->belongsTo('App\Localization');
    }
    
    public function kinship()
    {
        return $this->belongsTo('App\Kinship');
    }
    
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    
    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    public function image(){
        return $this->hasOne('App\File', 'id', 'image_id');
    }
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function log_users()
    {
        return $this->morphMany('App\LogUser', 'loguserable');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function authorizeRoles($permissions)
    {
        abort_unless($this->hasAnyRole($permissions), 401);
        return true;
    }

    public function hasAnyRole($permissions)
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                if ($this->hasRole($permission)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($permissions)) {
                return true; 
            }   
        }
        return false;
    }
    
    public function hasRole($permission)
    {
        $has = $this->role->permissions()->where('name', $permission)->first();
        if ($has || $this->role->name == 'Administrador') {
            return true;
        }
        return false;
    }

}
