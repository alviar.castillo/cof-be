<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    const NAME_SECTION = 'AGENDA';

    protected $fillable = [
        'date','day','start','end','duration','recurrent','status_id',
        'assesor_id','portal_id'
    ];

    public function assesor()
    {
        return $this->belongsTo('App\Assesor');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
    
    public function status()
    {
        return $this->belongsTo('App\Status', 'id', 'status_id');
    }
}
