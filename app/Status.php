<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    const AVAILABILITY = 'Agenda';
    const APPOINMENT = 'Cita';

    protected $fillable = [
        'name','type'
    ];
}
